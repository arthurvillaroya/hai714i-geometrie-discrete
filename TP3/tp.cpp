// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"

enum DisplayMode{ WIRE=0, SOLID=1, LIGHTED_WIRE=2, LIGHTED=3 };

struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    float areaTriangle(std::vector< Vec3 > & vertices) const{
        Vec3 ab = vertices[v[1]] - vertices[v[0]];
        Vec3 ac = vertices[v[2]] - vertices[v[0]];

        Vec3 vectoriel = Vec3::cross(ab,ac);

        return vectoriel.length()/2;
    }
    // membres indices des sommets du triangle:
    unsigned int v[3];
};


struct Mesh {
    std::vector< Vec3 > vertices; //array of mesh vertices positions
    std::vector< Vec3 > normals; //array of vertices normals useful for the display
    std::vector< Triangle > triangles; //array of mesh triangles
    std::vector< Vec3 > triangle_normals; //triangle normals to display face normals

    //Compute face normals for the display
    void computeTrianglesNormals(){

        triangle_normals.clear();
        triangle_normals.resize(triangles.size());

        for(long unsigned int i=0; i<triangles.size(); i++){
            Vec3 P1 = vertices[triangles[i].v[0]];
            Vec3 P2 = vertices[triangles[i].v[1]];
            Vec3 P3 = vertices[triangles[i].v[2]];

            Vec3 vectoriel = Vec3::cross((P2-P1), (P3-P1));
            vectoriel.normalize();

            triangle_normals[i] = vectoriel;
        }
    }

    //Compute vertices normals as the average of its incident faces normals
    void computeVerticesNormals(int weight_type){
        //Utiliser weight_type : 0 uniforme, 1 aire des triangles, 2 angle du triangle

        normals.clear();
        normals.resize(vertices.size());
        for(unsigned long int i = 0; i < normals.size(); i++){
            normals[i] = Vec3(0.0, 0.0, 0.0);
        }
        
        if(weight_type == 0){
            for(unsigned long int i = 0; i < triangles.size(); i++){
                normals[triangles[i].v[0]] += triangle_normals[i];
                normals[triangles[i].v[1]] += triangle_normals[i];
                normals[triangles[i].v[2]] += triangle_normals[i];
            }         
        }

        if(weight_type == 1){
            for(unsigned long int i = 0; i < triangles.size(); i++){
                normals[triangles[i].v[0]] += triangles[i].areaTriangle(vertices)*triangle_normals[i];
                normals[triangles[i].v[1]] += triangles[i].areaTriangle(vertices)*triangle_normals[i];
                normals[triangles[i].v[2]] += triangles[i].areaTriangle(vertices)*triangle_normals[i];
            } 
        }
 
        if((weight_type == 2) || (weight_type == 3)){
            
            for(unsigned long int i = 0; i < triangles.size(); i++){
                Vec3 ab = vertices[triangles[i].v[1]] - vertices[triangles[i].v[0]];
                Vec3 ac = vertices[triangles[i].v[2]] - vertices[triangles[i].v[0]];

                Vec3 ba = vertices[triangles[i].v[0]] - vertices[triangles[i].v[1]];
                Vec3 bc = vertices[triangles[i].v[2]] - vertices[triangles[i].v[1]];

                Vec3 ca = vertices[triangles[i].v[0]] - vertices[triangles[i].v[2]];
                Vec3 cb = vertices[triangles[i].v[1]] - vertices[triangles[i].v[2]];
                
                ab.normalize();
                ac.normalize();
                ba.normalize();
                bc.normalize();
                ca.normalize();
                cb.normalize();

                double scalaireA = Vec3::dot(ab,ac);
                double scalaireB = Vec3::dot(ba,bc);
                double scalaireC = Vec3::dot(ca,cb);

                if(weight_type == 2){
                    normals[triangles[i].v[0]] += triangles[i].areaTriangle(vertices)*acos(scalaireA)*triangle_normals[i];
                    normals[triangles[i].v[1]] += triangles[i].areaTriangle(vertices)*acos(scalaireB)*triangle_normals[i];
                    normals[triangles[i].v[2]] += triangles[i].areaTriangle(vertices)*acos(scalaireC)*triangle_normals[i];
                }
                else{
                    normals[triangles[i].v[0]] += acos(scalaireA)*triangle_normals[i];
                    normals[triangles[i].v[1]] += acos(scalaireB)*triangle_normals[i];
                    normals[triangles[i].v[2]] += acos(scalaireC)*triangle_normals[i];
                }
            } 
        }


        for (unsigned long int i = 0; i < normals.size(); i++)
        {
            normals[i].normalize();
        }
        //A faire : implémenter le calcul des normales par sommet comme la moyenne des normales des triangles incidents
        //Attention commencer la fonction par normals.clear();
        //Initializer le vecteur normals taille vertices.size() avec Vec3(0., 0., 0.)
        //Iterer sur les triangles

        //Pour chaque triangle i
        //Ajouter la normal au triangle à celle de chacun des sommets en utilisant des poids
        //0 uniforme, 1 aire du triangle, 2 angle du triangle

        //Iterer sur les normales et les normaliser
    }

    void computeNormals(int weight_type){
        computeTrianglesNormals();
        computeVerticesNormals(weight_type);
    }
};

//Transformation made of a rotation and translation
struct Transformation {
    Mat3 rotation;
    Vec3 translation;
};

//Basis ( origin, i, j ,k )
struct Basis {
    inline Basis ( Vec3 const & i_origin,  Vec3 const & i_i, Vec3 const & i_j, Vec3 const & i_k) {
        origin = i_origin; i = i_i ; j = i_j ; k = i_k;
    }

    inline Basis ( ) {
        origin = Vec3(0., 0., 0.);
        i = Vec3(1., 0., 0.) ; j = Vec3(0., 1., 0.) ; k = Vec3(0., 0., 1.);
    }
    Vec3 operator [] (unsigned int ib) {
        if(ib==0) return i;
        if(ib==1) return j;
        return k;}

    Vec3 origin;
    Vec3 i;
    Vec3 j;
    Vec3 k;
};

//Fonction à completer
void collect_one_ring (std::vector<Vec3> const & i_vertices,
                       std::vector< Triangle > const & i_triangles,
                       std::vector<std::vector<unsigned int> > & o_one_ring) {//one-ring of each vertex, i.e. a list of vertices with which it shares an edge
    //Initialiser le vecetur de o_one_ring de la taille du vecteur vertices
    //Parcourir les triangles et ajouter les voisins dans le 1-voisinage
    //Attention verifier que l'indice n'est pas deja present
    //Tous les points opposés dans le triangle sont reliés

	o_one_ring.resize(i_vertices.size());


	for (long unsigned int j = 0; j < i_triangles.size(); j++){
	   
	   for(int k = 0; k < 3; k++){

	   	    if ((std::count(o_one_ring[i_triangles[j][k]].begin(), o_one_ring[i_triangles[j][k]].end(), i_triangles[j][0]) == 0) && (i_triangles[j][0] != i_triangles[j][k])){
				
				o_one_ring[i_triangles[j][k]].push_back(i_triangles[j][0]);
	   	    }

	   	    if ((std::count(o_one_ring[i_triangles[j][k]].begin(), o_one_ring[i_triangles[j][k]].end(), i_triangles[j][1]) == 0) && (i_triangles[j][1] != i_triangles[j][k])){
				
				o_one_ring[i_triangles[j][k]].push_back(i_triangles[j][1]);
	   	    }

	   	    if ((std::count(o_one_ring[i_triangles[j][k]].begin(), o_one_ring[i_triangles[j][k]].end(), i_triangles[j][2]) == 0) && (i_triangles[j][2] != i_triangles[j][k])){
				
				o_one_ring[i_triangles[j][k]].push_back(i_triangles[j][2]);
	   	    }

	   	    //std::cout<<o_one_ring[i_triangles[j][k]].size()<<std::endl;	
	   	}
	} 
}

//Fonction à completer
void compute_vertex_valences (const std::vector<Vec3> & i_vertices,
                              const std::vector< Triangle > & i_triangles,
                              std::vector<unsigned int> & o_valences ) {
    
    std::vector<std::vector<unsigned int> >  o_one_ring;

    collect_one_ring(i_vertices, i_triangles, o_one_ring);
    
    for(long unsigned int i = 0; i < o_one_ring.size(); i++){
    	o_valences.push_back(o_one_ring[i].size());
    }
}

//Input mesh loaded at the launch of the application
Mesh mesh;
std::vector< float > mesh_valence_field; //normalized valence of each vertex

Basis basis;

bool display_normals;
bool display_smooth_normals;
bool display_mesh;
bool display_basis;
DisplayMode displayMode;
int weight_type;

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;

// ------------------------------------
// File I/O
// ------------------------------------
bool saveOFF( const std::string & filename ,
              std::vector< Vec3 > const & i_vertices ,
              std::vector< Vec3 > const & i_normals ,
              std::vector< Triangle > const & i_triangles,
              std::vector< Vec3 > const & i_triangle_normals ,
              bool save_normals = true ) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl ;

    unsigned int n_vertices = i_vertices.size() , n_triangles = i_triangles.size();
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << i_vertices[v][0] << " " << i_vertices[v][1] << " " << i_vertices[v][2] << " ";
        if (save_normals) myfile << i_normals[v][0] << " " << i_normals[v][1] << " " << i_normals[v][2] << std::endl;
        else myfile << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << i_triangles[f][0] << " " << i_triangles[f][1] << " " << i_triangles[f][2]<< " ";
        if (save_normals) myfile << i_triangle_normals[f][0] << " " << i_triangle_normals[f][1] << " " << i_triangle_normals[f][2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}

void openOFF( std::string const & filename,
              std::vector<Vec3> & o_vertices,
              std::vector<Vec3> & o_normals,
              std::vector< Triangle > & o_triangles,
              std::vector< Vec3 > & o_triangle_normals,
              bool load_normals = true )
{
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open())
    {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if( magic_s != "OFF" )
    {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices , n_faces , dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    o_vertices.clear();
    o_normals.clear();

    for( int v = 0 ; v < n_vertices ; ++v )
    {
        float x , y , z ;

        myfile >> x >> y >> z ;
        o_vertices.push_back( Vec3( x , y , z ) );

        if( load_normals ) {
            myfile >> x >> y >> z;
            o_normals.push_back( Vec3( x , y , z ) );
        }
    }

    o_triangles.clear();
    o_triangle_normals.clear();
    for( int f = 0 ; f < n_faces ; ++f )
    {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if( n_vertices_on_face == 3 )
        {
            unsigned int _v1 , _v2 , _v3;
            myfile >> _v1 >> _v2 >> _v3;

            o_triangles.push_back(Triangle( _v1, _v2, _v3 ));

            if( load_normals ) {
                float x , y , z ;
                myfile >> x >> y >> z;
                o_triangle_normals.push_back( Vec3( x , y , z ) );
            }
        }
        else if( n_vertices_on_face == 4 )
        {
            unsigned int _v1 , _v2 , _v3 , _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            o_triangles.push_back(Triangle(_v1, _v2, _v3 ));
            o_triangles.push_back(Triangle(_v1, _v3, _v4));
            if( load_normals ) {
                float x , y , z ;
                myfile >> x >> y >> z;
                o_triangle_normals.push_back( Vec3( x , y , z ) );
            }

        }
        else
        {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
            myfile.close();
            exit(1);
        }
    }

}

// ------------------------------------
// Application initialization
// ------------------------------------6

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glDisable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

    display_normals = false;
    display_mesh = true;
    display_smooth_normals = true;
    displayMode = LIGHTED;
    display_basis = false;
}


// ------------------------------------
// Rendering.
// ------------------------------------

void drawVector( Vec3 const & i_from, Vec3 const & i_to ) {

    glBegin(GL_LINES);
    glVertex3f( i_from[0] , i_from[1] , i_from[2] );
    glVertex3f( i_to[0] , i_to[1] , i_to[2] );
    glEnd();
}

void drawAxis( Vec3 const & i_origin, Vec3 const & i_direction ) {

    glLineWidth(4); // for example...
    drawVector(i_origin, i_origin + i_direction);
}

void drawReferenceFrame( Vec3 const & origin, Vec3 const & i, Vec3 const & j, Vec3 const & k ) {

    glDisable(GL_LIGHTING);
    glColor3f( 0.8, 0.2, 0.2 );
    drawAxis( origin, i );
    glColor3f( 0.2, 0.8, 0.2 );
    drawAxis( origin, j );
    glColor3f( 0.2, 0.2, 0.8 );
    drawAxis( origin, k );
    glEnable(GL_LIGHTING);

}

void drawReferenceFrame( Basis & i_basis ) {
    drawReferenceFrame( i_basis.origin, i_basis.i, i_basis.j, i_basis.k );
}

typedef struct {
    float r;       // ∈ [0, 1]
    float g;       // ∈ [0, 1]
    float b;       // ∈ [0, 1]
} RGB;



RGB scalarToRGB( float scalar_value ) //Scalar_value ∈ [0, 1]
{
    RGB rgb;
    float H = scalar_value*360., S = 1., V = 0.85,
            P, Q, T,
            fract;

    (H == 360.)?(H = 0.):(H /= 60.);
    fract = H - floor(H);

    P = V*(1. - S);
    Q = V*(1. - S*fract);
    T = V*(1. - S*(1. - fract));

    if      (0. <= H && H < 1.)
        rgb = (RGB){.r = V, .g = T, .b = P};
    else if (1. <= H && H < 2.)
        rgb = (RGB){.r = Q, .g = V, .b = P};
    else if (2. <= H && H < 3.)
        rgb = (RGB){.r = P, .g = V, .b = T};
    else if (3. <= H && H < 4.)
        rgb = (RGB){.r = P, .g = Q, .b = V};
    else if (4. <= H && H < 5.)
        rgb = (RGB){.r = T, .g = P, .b = V};
    else if (5. <= H && H < 6.)
        rgb = (RGB){.r = V, .g = P, .b = Q};
    else
        rgb = (RGB){.r = 0., .g = 0., .b = 0.};

    return rgb;
}

void drawSmoothTriangleMesh( Mesh const & i_mesh , bool draw_field = false ) {
    glBegin(GL_TRIANGLES);
    for(unsigned int tIt = 0 ; tIt < i_mesh.triangles.size(); ++tIt) {

        for(unsigned int i = 0 ; i < 3 ; i++) {
            const Vec3 & p = i_mesh.vertices[i_mesh.triangles[tIt][i]]; //Vertex position
            const Vec3 & n = i_mesh.normals[i_mesh.triangles[tIt][i]]; //Vertex normal

            if( draw_field && mesh_valence_field.size() > 0 ){
                RGB color = scalarToRGB( mesh_valence_field[i_mesh.triangles[tIt][i]] );
                glColor3f( color.r, color.g, color.b );
            }
            glNormal3f( n[0] , n[1] , n[2] );
            glVertex3f( p[0] , p[1] , p[2] );
        }
    }
    glEnd();

}

void drawTriangleMesh( Mesh const & i_mesh , bool draw_field = false  ) {
    glBegin(GL_TRIANGLES);
    for(unsigned int tIt = 0 ; tIt < i_mesh.triangles.size(); ++tIt) {
        const Vec3 & n = i_mesh.triangle_normals[ tIt ]; //Triangle normal
        for(unsigned int i = 0 ; i < 3 ; i++) {
            const Vec3 & p = i_mesh.vertices[i_mesh.triangles[tIt][i]]; //Vertex position

            if( draw_field ){
                RGB color = scalarToRGB( mesh_valence_field[i_mesh.triangles[tIt][i]] );
                glColor3f( color.r, color.g, color.b );
            }
            glNormal3f( n[0] , n[1] , n[2] );
            glVertex3f( p[0] , p[1] , p[2] );
        }
    }
    glEnd();

}

void drawMesh( Mesh const & i_mesh , bool draw_field = false ){
    if(display_smooth_normals)
        drawSmoothTriangleMesh(i_mesh, draw_field) ; //Smooth display with vertices normals
    else
        drawTriangleMesh(i_mesh, draw_field) ; //Display with face normals
}

void drawVectorField( std::vector<Vec3> const & i_positions, std::vector<Vec3> const & i_directions ) {
    glLineWidth(1.);
    for(unsigned int pIt = 0 ; pIt < i_directions.size() ; ++pIt) {
        Vec3 to = i_positions[pIt] + 0.02*i_directions[pIt];
        drawVector(i_positions[pIt], to);
    }
}

void drawNormals(Mesh const& i_mesh){

    if(display_smooth_normals){
        drawVectorField( i_mesh.vertices, i_mesh.normals );
    } else {
        std::vector<Vec3> triangle_baricenters;
        for ( const Triangle& triangle : i_mesh.triangles ){
            Vec3 triangle_baricenter (0.,0.,0.);
            for( unsigned int i = 0 ; i < 3 ; i++ )
                triangle_baricenter += i_mesh.vertices[triangle[i]];
            triangle_baricenter /= 3.;
            triangle_baricenters.push_back(triangle_baricenter);
        }

        drawVectorField( triangle_baricenters, i_mesh.triangle_normals );
    }
}
/******************************************************************************************************/
/****************************************CURVE*********************************************************/
/******************************************************************************************************/

void drawCurve(std::vector<Vec3> points){
    glBegin(GL_LINE_STRIP);
    unsigned long int size = points.size();

    for(unsigned long int i = 0; i < size; i++){
        glVertex3f(points[i][0], points[i][1], points[i][2]);
    }

    glEnd();
}

void hermiteCubicCurve(std::vector<Vec3> & points, Vec3 P0, Vec3 P1, Vec3 V0, Vec3 V1, int nbU){ 
    points.clear();

    Vec3 d = P0;
    Vec3 c = V0;
    Vec3 b = -3*P0 + 3*P1 - 2*V0 - V1;
    Vec3 a = 2*P0 - 2*P1 + V0 + V1;

    float separate = 0;

    float x;
    float y;
    float z;
    while(separate <= 1){
        x = a[0]*pow(separate,3) + b[0]*pow(separate,2) + c[0]*separate + d[0];
        y = a[1]*pow(separate,3) + b[1]*pow(separate,2) + c[1]*separate + d[1];
        z = a[2]*pow(separate,3) + b[2]*pow(separate,2) + c[2]*separate + d[2];
        points.push_back(Vec3(x,y,z));

        separate += 1/float(nbU);
    }
}

int factoriel(unsigned long int n){
    if(n==0){
        return 1;
    }
    return n * factoriel(n-1);
}

float bernstein(unsigned long int n, unsigned long int i, double u){
    float premier = factoriel(n)/(float(factoriel(i)* factoriel(n-i)));
    float deuxieme = pow(u,i);
    float troisieme = pow((1-u), (n-i));

    return premier * deuxieme * troisieme;
}

Vec3 polyBernstein(std::vector<Vec3> const& controles, double separate){
    Vec3 somme = Vec3(0,0,0);
    for(unsigned long int i = 0; i<controles.size(); i++){
        somme += bernstein(controles.size()-1, i, separate)*controles[i];
    }

    return somme;
}

void BezierCurveByBernstein(std::vector<Vec3> & points, std::vector<Vec3> const& controles, int nbU){
    points.clear();

    double separate = 1/double(nbU);
    Vec3 somme;
    for(int i = 0; i <= nbU; i++){
        somme = polyBernstein(controles, separate*i);
        points.push_back(somme);
    }   
}

Vec3 casteljau(unsigned long int k, int i, double separate, std::vector<Vec3> const& controles, int a, std::vector<std::vector<Vec3>> &pointsInter){
    if(k==0){
        return controles[i];
    }
    
    if(a==36){
        for(int c = 0; c < pointsInter[k+1].size()-1; c++){
            pointsInter[k].push_back((1-separate)*pointsInter[k+1][c] + (separate*pointsInter[k+1][c+1]));
        }
    }

    Vec3 Ret;
    Ret = (1-separate) * casteljau(k-1, i, separate, controles, a, pointsInter) + separate * casteljau(k-1, i+1, separate, controles, a, pointsInter);
    

    return Ret;
}

void BeziersCurveByCasteljau(std::vector<Vec3> & points, std::vector<Vec3> const& controles, int nbU, std::vector<std::vector<Vec3>> &pointsInter){
    points.clear();

    double separate = 1/double(nbU);
    unsigned long int k = controles.size()-1;

    for(int i = 0; i <= nbU; i++){
        points.push_back(casteljau(k, 0, separate*i, controles, i, pointsInter));
    }

}

std::vector<std::vector<Vec3>> pointsInter;
std::vector<Vec3> controles;
std::vector<Vec3> points;

//Draw fonction
void draw () {

    if(displayMode == LIGHTED || displayMode == LIGHTED_WIRE){

        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_LIGHTING);

    }  else if(displayMode == WIRE){

        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glDisable (GL_LIGHTING);

    }  else if(displayMode == SOLID ){
        glDisable (GL_LIGHTING);
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

    }

    //glColor3f(0.8,1,0.8);
    //drawMesh(mesh, true);
    glColor3f(0.0,1.0,0.0);
    drawCurve(points);
    glColor3f(1.0,0.0,0.0);
    drawCurve(controles);
    glColor3f(0.0,0.5,1.0);
    for(int i = 1; i < pointsInter.size(); i++){
        drawCurve(pointsInter[i]);
    }

    if(displayMode == SOLID || displayMode == LIGHTED_WIRE){
        glEnable (GL_POLYGON_OFFSET_LINE);
        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth (1.0f);
        glPolygonOffset (-2.0, 1.0);

        glColor3f(0.,0.,0.);
        drawMesh(mesh, false);

        glDisable (GL_POLYGON_OFFSET_LINE);
        glEnable (GL_LIGHTING);
    }



    glDisable(GL_LIGHTING);
    if(display_normals){
        glColor3f(1.,0.,0.);
        drawNormals(mesh);
    }

    if( display_basis ){
        drawReferenceFrame(basis);
    }
    glEnable(GL_LIGHTING);


}

void changeDisplayMode(){
    if(displayMode == LIGHTED)
        displayMode = LIGHTED_WIRE;
    else if(displayMode == LIGHTED_WIRE)
        displayMode = SOLID;
    else if(displayMode == SOLID)
        displayMode = WIRE;
    else
        displayMode = LIGHTED;
}

void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

// ------------------------------------
// User inputs
// ------------------------------------
//Keyboard event
void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;


    case 'w': //Change le mode d'affichage
        changeDisplayMode();
        break;


    case 'b': //Toggle basis display
        display_basis = !display_basis;
        break;

    case 'n': //Press n key to display normals
        display_normals = !display_normals;
        break;

    case '1': //Toggle loaded mesh display
        display_mesh = !display_mesh;
        break;

    case 's': //Switches between face normals and vertices normals
        display_smooth_normals = !display_smooth_normals;
        break;

    case '+': //Changes weight type: 0 uniforme, 1 aire des triangles, 2 angle du triangle
        weight_type ++;
        if(weight_type == 4) weight_type = 0;
        mesh.computeVerticesNormals(weight_type); //recalcul des normales avec le type de poids choisi
        break;

    default:
        break;
    }
    idle ();
}

//Mouse events
void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }

    idle ();
}

//Mouse motion, update camera
void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}


void NormalisationValences(std::vector<unsigned int> const &  valences, std::vector<float> & valencesN){
	float max = 0;
	for(long unsigned int i = 0; i < valences.size(); i++){
		if(max < valences[i]){
			max = valences[i];
		}
	}

	for(long unsigned int i = 0; i < valences.size(); i++){
		valencesN.push_back(valences[i]/max);
	}
}

// ------------------------------------
// Start of graphical application
// ------------------------------------
int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }

    /*Vec3 P0 = Vec3(0,0,0);
    Vec3 P1 = Vec3(2,0,0);
    Vec3 V0 = Vec3(1,1,0);
    Vec3 V1 = Vec3(1,1,0);

    hermiteCubicCurve(points, P0, P1, V0, V1, 100);*/
    controles.push_back(Vec3(0,0,0));
    controles.push_back(Vec3(1,0.7,0));
    controles.push_back(Vec3(1.25,-1,0));
    controles.push_back(Vec3(2,0.2,0));
    
    pointsInter.resize(5);
    for(int i = 0; i < controles.size(); i++){
        pointsInter[4].push_back(controles[i]);
    }

    BeziersCurveByCasteljau(points,controles,100, pointsInter);

    for(int i = 0; i<pointsInter.size(); i++){
        std::cout<<pointsInter[i].size()<<std::endl;
    }

    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP HAI702I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    //Mesh loaded with precomputed normals
    openOFF("data/elephant_n.off", mesh.vertices, mesh.normals, mesh.triangles, mesh.triangle_normals);

    basis = Basis();

    glutMainLoop ();
    return EXIT_SUCCESS;
}

