// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include <limits>

#define EPSILON 1

enum DisplayMode{ WIRE=0, SOLID=1, LIGHTED_WIRE=2, LIGHTED=3 };

struct Triangle {
    inline Triangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline Triangle (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline Triangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    unsigned int & operator [] (unsigned int iv) { return v[iv]; }
    unsigned int operator [] (unsigned int iv) const { return v[iv]; }
    inline virtual ~Triangle () {}
    inline Triangle & operator = (const Triangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    float areaTriangle(std::vector< Vec3 > & vertices) const{
        Vec3 ab = vertices[v[1]] - vertices[v[0]];
        Vec3 ac = vertices[v[2]] - vertices[v[0]];

        Vec3 vectoriel = Vec3::cross(ab,ac);

        return vectoriel.length()/2;
    }
    // membres indices des sommets du triangle:
    unsigned int v[3];
};


struct Mesh {
    std::vector< Vec3 > vertices; //array of mesh vertices positions
    std::vector< Vec3 > normals; //array of vertices normals useful for the display
    std::vector< Triangle > triangles; //array of mesh triangles
    std::vector< Vec3 > triangle_normals; //triangle normals to display face normals

    //Compute face normals for the display
    void computeTrianglesNormals(){

        triangle_normals.clear();
        triangle_normals.resize(triangles.size());

        for(long unsigned int i=0; i<triangles.size(); i++){
            Vec3 P1 = vertices[triangles[i].v[0]];
            Vec3 P2 = vertices[triangles[i].v[1]];
            Vec3 P3 = vertices[triangles[i].v[2]];

            Vec3 vectoriel = Vec3::cross((P2-P1), (P3-P1));
            vectoriel.normalize();

            triangle_normals[i] = vectoriel;
        }
    }
    void computeNormals(int weight_type){
        computeTrianglesNormals();
    }
};

//Transformation made of a rotation and translation
struct Transformation {
    Mat3 rotation;
    Vec3 translation;
};

//Basis ( origin, i, j ,k )
struct Basis {
    inline Basis ( Vec3 const & i_origin,  Vec3 const & i_i, Vec3 const & i_j, Vec3 const & i_k) {
        origin = i_origin; i = i_i ; j = i_j ; k = i_k;
    }

    inline Basis ( ) {
        origin = Vec3(0., 0., 0.);
        i = Vec3(1., 0., 0.) ; j = Vec3(0., 1., 0.) ; k = Vec3(0., 0., 1.);
    }
    Vec3 operator [] (unsigned int ib) {
        if(ib==0) return i;
        if(ib==1) return j;
        return k;}

    Vec3 origin;
    Vec3 i;
    Vec3 j;
    Vec3 k;
};

struct Point{
    Vec3 pos;

    bool estDansSphere(Vec3 centre, double rayon){
        double eq = (centre[0] - pos[0])*(centre[0] - pos[0]) + (centre[1] - pos[1])*(centre[1] - pos[1]) + (centre[2] - pos[2])*(centre[2] - pos[2]); 
        return (eq <= rayon*rayon);
    }
    bool estDansCylindre(Vec3 axisOrigin, Vec3 axisVector, double rayon){
        bool a = (Vec3::dot((pos - axisOrigin), (axisVector - axisOrigin)) >= 0);
        bool b = (Vec3::dot((pos - axisVector), (axisVector - axisOrigin)) <= 0);
        bool c = ((Vec3::cross((pos - axisVector), (axisVector - axisOrigin)).length())/(axisVector - axisOrigin).length()) <= rayon;

        return a && b && c;
    }
};

struct Voxel
{
    Point points[8];
    bool pointSigne[8];
    bool doitAfficher;

    void init(Vec3 centre, double longueurCote){
        double distCentre = longueurCote/2;

        points[0].pos = Vec3((centre[0] - distCentre), (centre[1] - distCentre), (centre[2] - distCentre));
        points[1].pos = Vec3((centre[0] + distCentre), (centre[1] - distCentre), (centre[2] - distCentre));
        points[2].pos = Vec3((centre[0] - distCentre), (centre[1] + distCentre), (centre[2] - distCentre));
        points[3].pos = Vec3((centre[0] + distCentre), (centre[1] + distCentre), (centre[2] - distCentre));
        points[4].pos = Vec3((centre[0] - distCentre), (centre[1] - distCentre), (centre[2] + distCentre));
        points[5].pos = Vec3((centre[0] + distCentre), (centre[1] - distCentre), (centre[2] + distCentre));
        points[6].pos = Vec3((centre[0] - distCentre), (centre[1] + distCentre), (centre[2] + distCentre));
        points[7].pos = Vec3((centre[0] + distCentre), (centre[1] + distCentre), (centre[2] + distCentre));

    }

    void signSphere(Vec3 centre, double rayon){
        for(int i = 0; i < 8; i++){
            pointSigne[i] = points[i].estDansSphere(centre, rayon);
        }
    }

    void signCylindre(Vec3 axisOrigin, Vec3 axisVector, double rayon){
        for(int i = 0; i < 8; i++){
            pointSigne[i] = points[i].estDansCylindre(axisOrigin, axisVector, rayon);
        }
    }

    void signIntersection(Vec3 centre, double rayonSphere, Vec3 axisOrigin, Vec3 axisVector, double rayonCylindre){
        for(int i = 0; i < 8; i++){
            pointSigne[i] = points[i].estDansCylindre(axisOrigin, axisVector, rayonCylindre) && points[i].estDansSphere(centre, rayonSphere);
        }
    }

    void signSousSphereInCylindre(Vec3 centre, double rayonSphere, Vec3 axisOrigin, Vec3 axisVector, double rayonCylindre){
        for(int i = 0; i < 8; i++){
            pointSigne[i] = points[i].estDansCylindre(axisOrigin, axisVector, rayonCylindre) && !(points[i].estDansSphere(centre, rayonSphere));
        }
    }

    void signUnion(Vec3 centre, double rayonSphere, Vec3 axisOrigin, Vec3 axisVector, double rayonCylindre){
        for(int i = 0; i < 8; i++){
            pointSigne[i] = points[i].estDansCylindre(axisOrigin, axisVector, rayonCylindre) || points[i].estDansSphere(centre, rayonSphere);
        }
    }

    bool auMoinsPositif(){
        for(int i = 0; i < 8; i++){
            if(pointSigne[i]){return true;} 
        }

        return false;
    }

    void displayPoints(){
        glPointSize(20);
	    glBegin(GL_POINTS);
	    for(unsigned long int i = 0; i < 8; i++){
	    	glVertex3f(points[i].pos[0], points[i].pos[1], points[i].pos[2]);
	    }
	    glEnd();
    }
    
    void displayVoxel(){
        glBegin(GL_QUAD_STRIP); // FACE AVANT
            glVertex3f(points[0].pos[0], points[0].pos[1], points[0].pos[2]);
            glVertex3f(points[1].pos[0], points[1].pos[1], points[1].pos[2]);
            glVertex3f(points[2].pos[0], points[2].pos[1], points[2].pos[2]);
            glVertex3f(points[3].pos[0], points[3].pos[1], points[3].pos[2]);
        glEnd();
        glBegin(GL_QUAD_STRIP); // FACE GAUCHE
            glVertex3f(points[0].pos[0], points[0].pos[1], points[0].pos[2]);
            glVertex3f(points[2].pos[0], points[2].pos[1], points[2].pos[2]);
            glVertex3f(points[4].pos[0], points[4].pos[1], points[4].pos[2]);
            glVertex3f(points[6].pos[0], points[6].pos[1], points[6].pos[2]);
        glEnd();
        glBegin(GL_QUAD_STRIP); // FACE DROITE
            glVertex3f(points[1].pos[0], points[1].pos[1], points[1].pos[2]);
            glVertex3f(points[3].pos[0], points[3].pos[1], points[3].pos[2]);
            glVertex3f(points[5].pos[0], points[5].pos[1], points[5].pos[2]);
            glVertex3f(points[7].pos[0], points[7].pos[1], points[7].pos[2]);
        glEnd();
        glBegin(GL_QUAD_STRIP); // FACE ARRIERE
            glVertex3f(points[4].pos[0], points[4].pos[1], points[4].pos[2]);
            glVertex3f(points[5].pos[0], points[5].pos[1], points[5].pos[2]);
            glVertex3f(points[6].pos[0], points[6].pos[1], points[6].pos[2]);
            glVertex3f(points[7].pos[0], points[7].pos[1], points[7].pos[2]);
        glEnd();
        glBegin(GL_QUAD_STRIP); // FACE BAS
            glVertex3f(points[0].pos[0], points[0].pos[1], points[0].pos[2]);
            glVertex3f(points[1].pos[0], points[1].pos[1], points[1].pos[2]);
            glVertex3f(points[4].pos[0], points[4].pos[1], points[4].pos[2]);
            glVertex3f(points[5].pos[0], points[5].pos[1], points[5].pos[2]);
        glEnd();
        glBegin(GL_QUAD_STRIP); // FACE HAUT
            glVertex3f(points[2].pos[0], points[2].pos[1], points[2].pos[2]);
            glVertex3f(points[3].pos[0], points[3].pos[1], points[3].pos[2]);
            glVertex3f(points[6].pos[0], points[6].pos[1], points[6].pos[2]);
            glVertex3f(points[7].pos[0], points[7].pos[1], points[7].pos[2]);
        glEnd();
    }
};

struct Grille
{
    std::vector<Vec3> centres;
    std::vector<Voxel> voxels;
    Vec3 cube[8];
    
    void initFirstGrille(){
        centres.push_back(Vec3(0,0,0));
        Voxel first;
        first.init(centres[0], 3);
        voxels.push_back(first); 
    }

    void boundingBoxSphere(Vec3 centre, double rayon){
        double rayonE = rayon + EPSILON;
        cube[0] = Vec3((centre[0] - rayonE), (centre[1] - rayonE), (centre[2] - rayonE));
        cube[1] = Vec3((centre[0] + rayonE), (centre[1] - rayonE), (centre[2] - rayonE));
        cube[2] = Vec3((centre[0] - rayonE), (centre[1] + rayonE), (centre[2] - rayonE));
        cube[3] = Vec3((centre[0] + rayonE), (centre[1] + rayonE), (centre[2] - rayonE));
        cube[4] = Vec3((centre[0] - rayonE), (centre[1] - rayonE), (centre[2] + rayonE));
        cube[5] = Vec3((centre[0] + rayonE), (centre[1] - rayonE), (centre[2] + rayonE));
        cube[6] = Vec3((centre[0] - rayonE), (centre[1] + rayonE), (centre[2] + rayonE));
        cube[7] = Vec3((centre[0] + rayonE), (centre[1] + rayonE), (centre[2] + rayonE));
    }

    void boundingBoxCylindre(Vec3 axisOrigin, Vec3 axisVector, double rayon){
        double rayonE = rayon + EPSILON;
        cube[0] = Vec3((axisOrigin[0] - rayonE), (axisOrigin[1] - rayonE), (axisOrigin[2] - rayonE));
        cube[1] = Vec3((axisOrigin[0] + rayonE), (axisOrigin[1] - rayonE), (axisOrigin[2] - rayonE));
        cube[2] = Vec3((axisOrigin[0] - rayonE), (axisOrigin[1] + rayonE), (axisOrigin[2] - rayonE));
        cube[3] = Vec3((axisOrigin[0] + rayonE), (axisOrigin[1] + rayonE), (axisOrigin[2] - rayonE));
        cube[4] = Vec3((axisVector[0] - rayonE), (axisVector[1] - rayonE), (axisVector[2] + rayonE));
        cube[5] = Vec3((axisVector[0] + rayonE), (axisVector[1] - rayonE), (axisVector[2] + rayonE));
        cube[6] = Vec3((axisVector[0] - rayonE), (axisVector[1] + rayonE), (axisVector[2] + rayonE));
        cube[7] = Vec3((axisVector[0] + rayonE), (axisVector[1] + rayonE), (axisVector[2] + rayonE));
    }

    void creatCenters(double resolution){
        double Xmin = cube[0][0];
        double Xmax = cube[7][0];
        double Ymin = cube[0][1];
        double Ymax = cube[7][1];
        double Zmin = cube[0][2];
        double Zmax = cube[7][2];

        for(double x = Xmin; x < Xmax; x += resolution){
            for(double y = Ymin; y < Ymax; y += resolution){
                for(double z = Zmin; z < Zmax; z += resolution){
                    centres.push_back(Vec3(x,y,z));
                }
            }
        }
    }

    void constructGrid(double resolution){
        creatCenters(resolution);
        unsigned long int size = centres.size();
        for(unsigned long int i = 0; i < size; i++){
            Voxel temp;
            temp.init(centres[i], resolution);
            voxels.push_back(temp);
        }
    }

    void displayVoxels(){
        for(unsigned long int i = 0; i < voxels.size(); i++){
            if(voxels[i].doitAfficher){
                voxels[i].displayVoxel();
            }
        }   
    }

    void DisplaySphereVolumic(Vec3 centre, double rayon, double resolution){
        boundingBoxSphere(centre, rayon);
        constructGrid(resolution);
        for(unsigned long int i = 0; i < voxels.size(); i++){
            voxels[i].signSphere(centre, rayon);
            voxels[i].doitAfficher = voxels[i].auMoinsPositif();
        }
    }

    void DisplayCylindreVolumic(Vec3 axisOrigin, Vec3 axisVector, double rayon, double resolution){
        boundingBoxCylindre(axisOrigin, axisVector, rayon);
        constructGrid(resolution);
        for(unsigned long int i = 0; i < voxels.size(); i++){
            voxels[i].signCylindre(axisOrigin, axisVector, rayon);
            voxels[i].doitAfficher = voxels[i].auMoinsPositif();
        }
    }

    void Display_INTERSECTION_SphereCylinder(Vec3 centreSphere, double rayonSphere, Vec3 axisOriginCylinder, Vec3 axisVectorCylinder, double rayonCylinder, double resolution){
        boundingBoxCylindre(axisOriginCylinder, axisVectorCylinder, rayonCylinder);
        constructGrid(resolution);
        boundingBoxSphere(centreSphere, rayonSphere);
        constructGrid(resolution);
        for(unsigned long int i = 0; i < voxels.size(); i++){
            voxels[i].signIntersection(centreSphere, rayonSphere, axisOriginCylinder, axisVectorCylinder, rayonCylinder);
            voxels[i].doitAfficher = voxels[i].auMoinsPositif();
        }
    }

    void Display_SOUSTRACTION_SphereCylinder(Vec3 centreSphere, double rayonSphere, Vec3 axisOriginCylinder, Vec3 axisVectorCylinder, double rayonCylinder, double resolution){
        boundingBoxCylindre(axisOriginCylinder, axisVectorCylinder, rayonCylinder);
        constructGrid(resolution);
        boundingBoxSphere(centreSphere, rayonSphere);
        constructGrid(resolution);
        for(unsigned long int i = 0; i < voxels.size(); i++){
            voxels[i].signSousSphereInCylindre(centreSphere, rayonSphere, axisOriginCylinder, axisVectorCylinder, rayonCylinder);
            voxels[i].doitAfficher = voxels[i].auMoinsPositif();
        }
    }

    void Display_UNION_SphereCylinder(Vec3 centreSphere, double rayonSphere, Vec3 axisOriginCylinder, Vec3 axisVectorCylinder, double rayonCylinder, double resolution){
        boundingBoxCylindre(axisOriginCylinder, axisVectorCylinder, rayonCylinder);
        constructGrid(resolution);
        boundingBoxSphere(centreSphere, rayonSphere);
        constructGrid(resolution);
        for(unsigned long int i = 0; i < voxels.size(); i++){
            voxels[i].signUnion(centreSphere, rayonSphere, axisOriginCylinder, axisVectorCylinder, rayonCylinder);
            voxels[i].doitAfficher = voxels[i].auMoinsPositif();
        }
    }
};


//Input mesh loaded at the launch of the application

std::vector< float > mesh_valence_field; //normalized valence of each vertex

Basis basis;

bool display_normals;
bool display_smooth_normals;
bool display_mesh;
bool display_basis;
DisplayMode displayMode;
int weight_type;

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;

// ------------------------------------
// Application initialization
// ------------------------------------6

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glDisable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

    display_normals = false;
    display_mesh = true;
    display_smooth_normals = true;
    displayMode = LIGHTED;
    display_basis = false;
}


// ------------------------------------
// Rendering.
// ------------------------------------

void drawVector( Vec3 const & i_from, Vec3 const & i_to ) {

    glBegin(GL_LINES);
    glVertex3f( i_from[0] , i_from[1] , i_from[2] );
    glVertex3f( i_to[0] , i_to[1] , i_to[2] );
    glEnd();
}

void drawAxis( Vec3 const & i_origin, Vec3 const & i_direction ) {

    glLineWidth(4); // for example...
    drawVector(i_origin, i_origin + i_direction);
}

void drawReferenceFrame( Vec3 const & origin, Vec3 const & i, Vec3 const & j, Vec3 const & k ) {

    glDisable(GL_LIGHTING);
    glColor3f( 0.8, 0.2, 0.2 );
    drawAxis( origin, i );
    glColor3f( 0.2, 0.8, 0.2 );
    drawAxis( origin, j );
    glColor3f( 0.2, 0.2, 0.8 );
    drawAxis( origin, k );
    glEnable(GL_LIGHTING);

}

void drawReferenceFrame( Basis & i_basis ) {
    drawReferenceFrame( i_basis.origin, i_basis.i, i_basis.j, i_basis.k );
}

typedef struct {
    float r;       // ∈ [0, 1]
    float g;       // ∈ [0, 1]
    float b;       // ∈ [0, 1]
} RGB;



RGB scalarToRGB( float scalar_value ) //Scalar_value ∈ [0, 1]
{
    RGB rgb;
    float H = scalar_value*360., S = 1., V = 0.85,
            P, Q, T,
            fract;

    (H == 360.)?(H = 0.):(H /= 60.);
    fract = H - floor(H);

    P = V*(1. - S);
    Q = V*(1. - S*fract);
    T = V*(1. - S*(1. - fract));

    if      (0. <= H && H < 1.)
        rgb = (RGB){.r = V, .g = T, .b = P};
    else if (1. <= H && H < 2.)
        rgb = (RGB){.r = Q, .g = V, .b = P};
    else if (2. <= H && H < 3.)
        rgb = (RGB){.r = P, .g = V, .b = T};
    else if (3. <= H && H < 4.)
        rgb = (RGB){.r = P, .g = Q, .b = V};
    else if (4. <= H && H < 5.)
        rgb = (RGB){.r = T, .g = P, .b = V};
    else if (5. <= H && H < 6.)
        rgb = (RGB){.r = V, .g = P, .b = Q};
    else
        rgb = (RGB){.r = 0., .g = 0., .b = 0.};

    return rgb;
}
void drawVectorField( std::vector<Vec3> const & i_positions, std::vector<Vec3> const & i_directions ) {
    glLineWidth(1.);
    for(unsigned int pIt = 0 ; pIt < i_directions.size() ; ++pIt) {
        Vec3 to = i_positions[pIt] + 0.02*i_directions[pIt];
        drawVector(i_positions[pIt], to);
    }
}
void drawNormals(Mesh const& i_mesh){

    if(display_smooth_normals){
        drawVectorField( i_mesh.vertices, i_mesh.normals );
    } else {
        std::vector<Vec3> triangle_baricenters;
        for ( const Triangle& triangle : i_mesh.triangles ){
            Vec3 triangle_baricenter (0.,0.,0.);
            for( unsigned int i = 0 ; i < 3 ; i++ )
                triangle_baricenter += i_mesh.vertices[triangle[i]];
            triangle_baricenter /= 3.;
            triangle_baricenters.push_back(triangle_baricenter);
        }

        drawVectorField( triangle_baricenters, i_mesh.triangle_normals );
    }
}

Grille mainGrille;
//Draw fonction
void draw () {

    if(displayMode == LIGHTED || displayMode == LIGHTED_WIRE){

        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_LIGHTING);

    }  else if(displayMode == WIRE){

        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glDisable (GL_LIGHTING);

    }  else if(displayMode == SOLID ){
        glDisable (GL_LIGHTING);
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

    }

    glColor3f(0,0.7,0.6);    
    mainGrille.displayVoxels();

    if(displayMode == SOLID || displayMode == LIGHTED_WIRE){
        glEnable (GL_POLYGON_OFFSET_LINE);
        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth (1.0f);
        glPolygonOffset (-2.0, 1.0);

        glColor3f(0.,0.,0.);

        glDisable (GL_POLYGON_OFFSET_LINE);
        glEnable (GL_LIGHTING);
    }



    glDisable(GL_LIGHTING);
    if(display_normals){
        glColor3f(1.,0.,0.);
    }

    if( display_basis ){
        drawReferenceFrame(basis);
    }
    glEnable(GL_LIGHTING);
}

void changeDisplayMode(){
    if(displayMode == LIGHTED)
        displayMode = LIGHTED_WIRE;
    else if(displayMode == LIGHTED_WIRE)
        displayMode = SOLID;
    else if(displayMode == SOLID)
        displayMode = WIRE;
    else
        displayMode = LIGHTED;
}

void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

// ------------------------------------
// User inputs
// ------------------------------------
//Keyboard event
void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;


    case 'w': //Change le mode d'affichage
        changeDisplayMode();
        break;

    case 'b': //Toggle basis display
        display_basis = !display_basis;
        break;

    case 'n': //Press n key to display normals
        display_normals = !display_normals;
        break;

    case '1': //Toggle loaded mesh display
        display_mesh = !display_mesh;
        break;

    case '+': //Changes weight type: 0 uniforme, 1 aire des triangles, 2 angle du triangle
        weight_type ++;
        if(weight_type == 4) weight_type = 0;
        //mesh.computeVerticesNormals(weight_type); //recalcul des normales avec le type de poids choisi
        break;

    default:
        break;
    }
    idle ();
}

//Mouse events
void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }

    idle ();
}

//Mouse motion, update camera
void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}


void NormalisationValences(std::vector<unsigned int> const &  valences, std::vector<float> & valencesN){
	float max = 0;
	for(long unsigned int i = 0; i < valences.size(); i++){
		if(max < valences[i]){
			max = valences[i];
		}
	}

	for(long unsigned int i = 0; i < valences.size(); i++){
		valencesN.push_back(valences[i]/max);
	}
}

// ------------------------------------
// Start of graphical application
// ------------------------------------

int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }

    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP HAI702I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    basis = Basis();
    mainGrille.Display_UNION_SphereCylinder(Vec3(3,10,0), 4, Vec3(0,0,0), Vec3(0,7,0), 4, 0.3);

    glutMainLoop ();
    return EXIT_SUCCESS;
}

